{-# LANGUAGE OverloadedStrings #-}

module Request where

import Network.HTTP.Req
import Data.ByteString (ByteString)
import qualified Data.ByteString.Char8 as BS
import Control.Monad.IO.Class

postRequest :: ByteString -> IO ByteString
postRequest body = runReq defaultHttpConfig $ do
    let url = https "boundvariable.space" /: "communicate"

    let headers = header "Authorization" "Bearer 8cd1314b-f873-4ab7-891f-781d814ed60e" <>
                  header "User-Agent" "Närke Nation"

    response <- req
        POST
        url
        (ReqBodyBs body)
        bsResponse
        headers

    return $ responseBody response
