Submission for lighting round.

It's not much, but the Haskell code contains a (mostly) working parser for the
language, along with a small command line client.

`spaceshipsolver.py` solves at least some of the spaceship challanges.

`proxy2.c` contains a bridge between the command line client and IRC. Since one
of our contestants failed to compile the Haskell code.

Known commands
--------------
- get index
- get scoreboard
- echo [message]
- get language_test

--------------------------------------------------

https://icfpcontest2024.github.io
