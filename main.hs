{-# Language ImportQualifiedPost, ScopedTypeVariables #-}

import Data.Map.Strict qualified as Map
import Data.Map.Strict (Map)
import Control.Monad.Reader (runReader)
import Eval (eval)
import Parse (parseICFPC)
import qualified Data.ByteString.Char8 as BS
import Control.Monad.Reader (Reader)
import Data.ByteString (ByteString)
import System.Console.Readline
import Control.Monad.Loops (unfoldM)

import System.IO

import Eval (Result (..))

import Request (postRequest)
import Serialize (encodeString)

processUser :: String -> IO ()
processUser line = do
    response <- postRequest . BS.pack . ('S':) . encodeString $ line
    let ast = parseICFPC . BS.unpack $ response
    let result = runReader (eval ast) Map.empty
    case result of
        RString s -> putStrLn s
        RNumber x -> putStrLn . show $ x
        RLambda arg expr _ -> putStrLn $ "λ" ++ show arg ++ " → " ++ show expr
    return ()

loop :: IO ()
loop = do
    line' <- readline "> "
    case line' of
        Nothing -> return ()
        Just line -> do
            addHistory line
            processUser line
            loop


main = do
    hSetBuffering stdin LineBuffering
    hSetBuffering stdout LineBuffering
    loop

