module Expression where

data Expression = Negate Expression
                | Not Expression
                | StringToInt Expression
                | IntToString Expression
                | Add Expression Expression
                | Sub Expression Expression
                | Mul Expression Expression
                | Div Expression Expression
                | Mod Expression Expression
                | Gt Expression Expression
                | Lt Expression Expression
                | Eq Expression Expression
                | Or Expression Expression
                | And Expression Expression
                | Concat Expression Expression
                | Take Expression Expression
                | Drop Expression Expression
                | Apply Expression Expression
                | If Expression Expression Expression
                | Lambda Integer Expression
                | Variable Integer
                | Boolean Bool
                | Number Integer
                | Str String
                deriving (Show)


