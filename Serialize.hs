module Serialize where

import Data.Char (ord, chr)
import Expression
import Data.List (elemIndex)
import Data.Maybe (fromJust)

encodeNumber :: Integer -> String
encodeNumber x = [chr(fromInteger a + 33) | a <- [mod (div x (94^b)) 94 | b<-[y,y-1..0]]]
    where y = floor $ logBase 94.0 (fromInteger x)

alphabet = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789!\"#$%&'()*+,-./:;<=>?@[\\]^_`|~ \n"

encodeString :: String -> String
encodeString s = [ chr $ (fromJust $ elemIndex c alphabet) + 33 | c <- s ]

decodeString :: String -> String
decodeString s = [ alphabet !! ((ord c) - 33) | c <- s ]

serialize (Number x) = 'I':encodeNumber x
serialize (Str x)          = 'S':encodeString x
serialize (Boolean True)   = "T"
serialize (Boolean False)  = "F"
serialize (Variable x)     = 'V':(fmap (chr . (+ 33) . ord) $ encodeNumber x)
serialize (Lambda x y)     = "L"    ++ fmap (chr . (+ 33) . ord) (encodeNumber x) ++ " " ++ serialize y
serialize (Negate x)       = "U- "  ++ serialize x
serialize (Not x)          = "U! "  ++ serialize x
serialize (StringToInt x)  = "U# "  ++ serialize x
serialize (IntToString  x) = "U$ "  ++ serialize x
serialize (Add x y)        = "B+ "  ++ serialize x ++ " " ++ serialize y
serialize (Sub x y)        = "B- "  ++ serialize x ++ " " ++ serialize y
serialize (Mul x y)        = "B* "  ++ serialize x ++ " " ++ serialize y
serialize (Div x y)        = "B/ "  ++ serialize x ++ " " ++ serialize y
serialize (Mod x y)        = "B% "  ++ serialize x ++ " " ++ serialize y
serialize (Gt x y)         = "B> "  ++ serialize x ++ " " ++ serialize y
serialize (Lt x y)         = "B< "  ++ serialize x ++ " " ++ serialize y
serialize (Eq x y)         = "B= "  ++ serialize x ++ " " ++ serialize y
serialize (Or x y)         = "B| "  ++ serialize x ++ " " ++ serialize y
serialize (And x y)        = "B& "  ++ serialize x ++ " " ++ serialize y
serialize (Concat x y)     = "B++ " ++ serialize x ++ " " ++ serialize y
serialize (Take x y)       = "BT "  ++ serialize x ++ " " ++ serialize y
serialize (Drop x y)       = "BD "  ++ serialize x ++ " " ++ serialize y
serialize (Apply x y)      = "B$ "  ++ serialize x ++ " " ++ serialize y
serialize (If x y z)       = "? "   ++ serialize x ++ " " ++ serialize y ++ " " ++ serialize z

