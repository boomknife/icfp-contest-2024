#include <libircclient/libircclient.h>
#include <libircclient/libirc_rfcnumeric.h>
#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <stdbool.h>
#include <pthread.h>
#include <errno.h>

int to_child[2], from_child[2];

struct io {
	FILE *in, *out;
	irc_session_t *session;
};

FILE *TOCHILD;

#define READ 0
#define WRITE 1

void *listen_child (void *inp);

void event_connect (
		irc_session_t *session,
		const char *event,
		const char *origin,
		const char **params,
		unsigned int count) {
	printf("Connect!\n");
	fflush(stdout);

	irc_cmd_join(session, "##hugo-test", NULL);
}

void event_numeric (
		irc_session_t *session,
		unsigned int event,
		const char *origin,
		const char **params,
		unsigned int count) {
	printf("Numeric! %u\n", event);
	fflush(stdout);
}


void event_join (
		irc_session_t *session,
		const char *event,
		const char *origin,
		const char **params,
		unsigned int count) {
	printf("Join!\n");
	irc_cmd_msg(session, "##hugo-test", "I hope I'm welcome");
	fflush(stdout);
}

void event_channel (
		irc_session_t *session,
		const char *event,
		const char *origin,
		const char **params,
		unsigned int count) {
	printf("%s %s", origin, params[0]);
	if (count > 1) {
		printf(": %s", params[1]);
		fprintf(TOCHILD, "%s\n", params[1]);
		fflush(TOCHILD);
	}
	printf("\n");
	fflush(stdout);
}

int setup_irc(FILE *toChild, FILE *fromChild) {

	TOCHILD = toChild;

	irc_callbacks_t callbacks = {
		.event_connect = &event_connect,
		.event_numeric = &event_numeric,
		.event_join    = &event_join,
		.event_channel = &event_channel,
	};

	irc_session_t *session = irc_create_session(&callbacks);
	if (!session) {
		fprintf(stderr, "Failed creating session\n");
		return 1;
	}

	irc_option_set(session, LIBIRC_OPTION_STRIPNICKS);

	if (irc_connect(session, "irc.libera.chat", 6667,
				NULL, /* Server password */
				"hugo2",  /* Server nick */
				"hugo", /* Local username */
				NULL)) {
		fprintf(stderr, "Failed connecting: %s\n", irc_strerror(irc_errno(session)));
		return 2;
	}



	pthread_attr_t attr;
	pthread_attr_init(&attr);
	pthread_t thread;

	struct io pipes = {
		.in = fromChild,
		.out = toChild,
		.session = session,
	};
	pthread_create(&thread, &attr, 
			&listen_child, &pipes);

	if (irc_run (session)) {
		fprintf(stderr, "Failed running: %s\n", irc_strerror(irc_errno(session)));
		return 3;
	}

	return 0;
}

void *listen_child (void *inp) {
	struct io *data = (struct io*) inp;

	printf("Thread live\n");
	fflush(stdout);

	while (true) {
		char *line = NULL;
		size_t n;
		printf("{");
		fflush(data->out);
		ssize_t len = getline(&line, &n, data->in);
		line[len - 1] = '\0';
		irc_cmd_msg(data->session, "##hugo-test", line);
		// fprintf(data->out, "[%s]\n", line);
		printf("}");
		fflush(data->out);
		// fprintf(data->out, "irc.lysator.hugo *%s\n");
		free(line);
	}

	return NULL;
}

int main() {

	pipe (to_child);
	pipe (from_child);

	int pid = fork();

	switch (pid) {
		case -1:
			return 1;
		case 0: /* Child */

			dup2(from_child[WRITE], STDOUT_FILENO);
			dup2(to_child[READ], STDIN_FILENO);

			execlp("cabal", "cabal", "run", NULL);
			fprintf(stderr, "Exec failed: %s\n", strerror(errno));
			break;

		default: /* Parent */
			printf("Forked\n");
			/*
			pthread_attr_t attr;
			pthread_attr_init(&attr);
			pthread_t thread;
			*/

			FILE *toChild = fdopen(to_child[WRITE], "w");
			FILE *fromChild = fdopen(from_child[READ], "r");

			setlinebuf(toChild);
			setlinebuf(fromChild);

			/*
			struct io pipes = {
				.in = fromChild,
				.out = stdout,
				// .session = session,
			};
			pthread_create(&thread, &attr, 
					&listen_child, &pipes);
					*/

			/*
			while (true) {
				char *line = NULL;
				size_t n;
				printf("> ");
				fflush(stdout);
				getline(&line, &n, stdin);
				// irc_cmd_msg(data->session, "##hugo-test", line);
				fprintf(toChild, "%s", line);
				fflush(toChild);
				// fprintf(data->out, "irc.lysator.hugo *%s\n");
				free(line);
			}
			*/

			// setup_irc();
			// t1
			// wait for input from subprocess
			// send it to irc
			// t2
			// wait for input from irc
			// send it to subprocess
	return setup_irc(toChild, fromChild);
	}

}
