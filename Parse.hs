module Parse where

import Text.ParserCombinators.Parsec
import Data.Char (ord, chr)
import Expression

parseBool :: GenParser Char () Expression
parseBool
    = (char 'T' >> (return $ Boolean True))
    <|> (char 'F' >> (return $ Boolean False))

decodeInteger :: String -> Integer
decodeInteger str = foldl ((+) . (* 94)) 0 [toInteger $ ord c - 33 | c <- str]

parseInteger :: GenParser Char () Expression
parseInteger = do
    char 'I'
    Number . decodeInteger <$> many (oneOf [chr 33..chr 126])

alphabet = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789!\"#$%&'()*+,-./:;<=>?@[\\]^_`|~ \n"

decodeString :: String -> String
decodeString msg = [alphabet !! (ord c - 33) | c <- msg]

parseString :: GenParser Char () Expression
parseString = do
    char 'S'
    s <- many (oneOf [chr 33..chr 126])
    return . Str $ decodeString s

parseUnary :: GenParser Char () Expression
parseUnary = do
    char 'U'
    c <- oneOf "-!#$"
    operand <- ws >> expressionParser
    case c of
        '-' -> return $ Negate operand
        '!' -> return $ Not operand
        '#' -> return $ StringToInt operand
        '$' -> return $ IntToString operand

parseBinary :: GenParser Char () Expression
parseBinary = do
    char 'B'
    operation <- oneOf "+-*/%<>=|&.TD$"
    op1 <- ws >> expressionParser
    op2 <- ws >> expressionParser
    case operation of
        '+' -> return $ Add op1 op2
        '-' -> return $ Sub op1 op2
        '*' -> return $ Mul op1 op2
        '/' -> return $ Div op1 op2
        '%' -> return $ Mod op1 op2
        '<' -> return $ Lt op1 op2
        '>' -> return $ Gt op1 op2
        '=' -> return $ Eq op1 op2
        '|' -> return $ Or op1 op2
        '&' -> return $ And op1 op2
        '.' -> return $ Concat op1 op2
        'T' -> return $ Take op1 op2
        'D' -> return $ Drop op1 op2
        '$' -> return $ Apply op1 op2

parseIf :: GenParser Char () Expression
parseIf = do
    char '?'
    op <- ws >> expressionParser
    e1 <- ws >> expressionParser
    e2 <- ws >> expressionParser
    return $ If op e1 e2

parseLambda :: GenParser Char () Expression
parseLambda = do
    char 'L'
    arg <- decodeInteger <$> many (oneOf [chr 33..chr 126])
    expr <- ws >> expressionParser
    return $ Lambda arg expr

parseVariable :: GenParser Char () Expression
parseVariable = do
    char 'v'
    arg <- decodeInteger <$> many (oneOf [chr 33..chr 126])
    return $ Variable arg

ws = many space

expressionParser = parseBool
            <|> parseInteger
            <|> parseString
            <|> parseUnary
            <|> parseBinary
            <|> parseIf
            <|> parseLambda
            <|> parseVariable
            <?> "Expression"

fromRight (Right x) = x

parseICFPC :: String -> Expression
parseICFPC str = fromRight $ parse expressionParser "" str
