#!/usr/bin/env python3

import numpy as np
import sys

def spaceshipsolver(coordinates):
    """
        :param coordinates:
        list of np arrays (of size 2).
    """
    velocity=np.array([0,0])
    moves=""
    curr_coords = np.array([0,0])
    while(coordinates):
        while((curr_coords!=coordinates[0]).any()):
            dist_after=coordinates[0]-curr_coords-velocity
            if(dist_after[0]>0):
                if(dist_after[1]>0):
                    moves+='9'
                    velocity += np.array([1,1])
                elif(dist_after[1]<0):
                    moves+='3'
                    velocity += np.array([1,-1])
                else:
                    moves+='6'
                    velocity += np.array([1,0])
            elif(dist_after[0]<0):
                if(dist_after[1]>0):
                    moves+='7'
                    velocity += np.array([-1,1])
                elif(dist_after[1]<0):
                    moves+='1'
                    velocity += np.array([-1,-1])
                else:
                    moves+='4'
                    velocity += np.array([-1,0])
            else:
                if(dist_after[1]>0):
                    moves+=('8')
                    velocity += np.array([0,1])
                elif(dist_after[1]<0):
                    moves+=('2')
                    velocity += np.array([0,-1])
                else:
                    moves+='5'
                    velocity += np.array([0,0])
            curr_coords+=velocity
        coordinates.pop(0)
    return moves

def list_coordinates_to_arrays(coordinates):
    return [np.array(x) for x in coordinates]


data = []
for line in sys.stdin.readlines():
    print(repr(line))
    data.append(np.array([int(s) for s in line.strip().split(' ')]))

print(spaceshipsolver(data))
