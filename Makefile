.PHONY: all clean

CFLAGS = -Wall -pedantic
LDLIBS = -lircclient $(shell pkg-config --libs openssl)

all: proxy2

clean:
	-rm main
